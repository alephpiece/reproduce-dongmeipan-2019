复现文献中的中子产生率计算结果。一共有两篇文献：

英文文献：https://doi.org/10.1080/15361055.2019.1570809

中文文献：http://dx.doi.org/10.7498/aps.68.20190440

两篇文献是同样的作者，但使用的公式似乎有点不同。计算所需的截面数据从文献的图中提取得到，其他数据均来自文献正文。
